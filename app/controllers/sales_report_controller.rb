class SalesReportController < ApplicationController


  def index
    if request.get?
@orders = Order.all
    else
      start_date = params[:start_date]
      end_date = params[:end_date]
      @orders = SalesReport.orders_by_product_in_range(start_date, end_date)
    end

  end


end
