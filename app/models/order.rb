
class Order < ApplicationRecord


  after_save :calculate_total

  belongs_to :employee
  has_many :carts, dependent: :destroy
  has_many :products, through: :carts

  accepts_nested_attributes_for :carts

  validates_presence_of :customer


  def calculate_total
      subtotal = self.carts.joins(:product).sum(:price)
      tax = subtotal * 0.10
      total = subtotal + tax
      update_columns(subtotal: subtotal, tax: tax, total: total)
  end
end
