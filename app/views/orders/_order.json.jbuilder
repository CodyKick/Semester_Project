json.extract! order, :id, :customer, :employee_id, :subtotal, :tax, :total, :date, :created_at, :updated_at
json.url order_url(order, format: :json)
