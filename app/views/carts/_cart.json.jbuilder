json.extract! cart, :id, :order_id, :product_id, :date, :created_at, :updated_at
json.url cart_url(cart, format: :json)
