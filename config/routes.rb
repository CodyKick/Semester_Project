Rails.application.routes.draw do
  get 'sales_report', to: "sales_report#index"
  post 'sales_report', to: "sales_report#index"


  root 'managers#home'
  resources :products
  resources :employees
  resources :managers
  resources :carts
  resources :orders
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
