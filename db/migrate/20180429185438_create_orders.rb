class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :customer
      t.references :employee, foreign_key: true
      t.decimal :subtotal
      t.decimal :tax
      t.decimal :total
      t.datetime :date

      t.timestamps
    end
  end
end
